# Isprime-project

# About
Using Docker to encapsulate a tool called Mkdocs
(​http://www.mkdocs.org/​) to produce and serve a website because we don’t want to
install Mkdocs locally.
The idea is to:
● Create a Git project that builds a Docker image. 
● This Docker image, when run, should accept a directory from your local
filesystem as input and use Mkdocs to produce and serve a website.
● This local directory is the root of a valid Mkdocs project with which this tool
can create the site.

Build and serve your existing mkdocs project over http